

Flashmaker Installation Instructions
====================================

Required Modules:
 - CCK
 - Imagefield
 

Dowload flashmaker from the project page and extract it to your modules folder
http://drupal.org/project/flashmaker

Install the Module and the Required Modules


Add an image field to the "Flashmaker Slide" content type
 1) In Drupal, Go to Content Management -> Content Types -> Flash Slide -> Manage fields
 2)  Under "Add Field" :
    Label: Slide Image 
    Fieldname: slide_image (must be exact)
    Type: Image
    Form Element: Image
 3) under Global settings: 
    - make the field required and have only 1 value (can't have multiple images per slide)

Imagecache (optional) - make slide images appear when editing a slideshow
  1) Install imagecache and imagecache ui
  2) Got to Site Building -> Imagecache -> add new preset
    Preset Name: slide_list (must be exact)
    Depending on your version, have the image scale (200px x 150px is a good setting)


Creating a slideshow
--------------------
Add permissions to "create flash content" and the like

Go to Create Content -> Flash Slideshow
Enter a name for your slideshow
Choose the viewer type:
Options right now are fade and autoviewer(see demo site for examples)
click submit

Add Slides to a Slideshow
------------------------- 
Go to Content -> Flash Slide
Enter a name for your slide
Chosse a slideshow for this slide to be a part of (has to already exist)
Choose a picture and click upload
Choose the weight (Determines orderof the slide)
Click Submit

Repeat for as many more slides as you need

View a slideshow
----------------
You can view the slideshow by viewing the full slideshow node or embed the flash with a theme function

Theme Function Example
----------------------
To see the fade viewer, replace 123 with the node id (node/123)
<?php print theme_flashmaker_fade(node_load(array('nid' => 123))); ?>

to see the autoviewer (a sideways scroller)
<?php print theme_flashmaker_autoviewer(node_load(array('nid' => 123))); ?>




















